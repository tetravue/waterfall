#ifndef TV_FILTER_H
#define TV_FILTER_H

#include "TvImage.h"
#include <random>

using namespace std;

// Functions return true on error

//Tools

template <class element, class weight>
struct TvWeightedElement
{
	TvWeightedElement() {};
	~TvWeightedElement() {};

	element m_value;
	weight m_weight;
};

// Collects neighbors and defers computation until populated, so good for research
// but not necessarily highest perf for some designs
template <class element, class weight>
class TvWeightedAccumulator
{
public:
	TvWeightedAccumulator() { Reset(); SetThreshold(); };
	TvWeightedAccumulator(int winsize, int depth = 1, weight weights_thresh = (weight)0.1f)
	{
		Reset();
		SetWindow(winsize, depth);
		SetThreshold(weights_thresh);
	};
	~TvWeightedAccumulator() {};
	inline void SetWindow(int winsize, int depth = 1)
	{
		assert(winsize > 0 && depth > 0);
		m_samples.resize(winsize * winsize * depth);
	};
	inline void Reset() { m_count = 0; }
	inline void SetThreshold(weight weights_thresh = (weight)0.1f) { m_weights_threshold = weights_thresh; };
	inline void AddSample(const element &v, const weight &w)
	{
		assert(m_count < m_samples.size()); // Full?
		m_samples[m_count].m_weight = w;
		m_samples[m_count].m_value = v * w;
		m_count++;
	};
	void SetThresholds(
		const element &thresh_min_blk,
		const element &thresh_max_blk)
	{
		m_thresh_min_blk = thresh_min_blk;
		m_thresh_max_blk = thresh_max_blk;
		m_slope_weight = 1.0f / (thresh_max_blk - thresh_min_blk);
	};
	void SetThresholds(
		const element &black,
		const element &white,
		const element &thresh_min_blk, // Todo: need type independent method
		const element &thresh_max_blk,
		const element &thresh_min_wht,
		const element &thresh_max_wht)
	{
		m_thresh_min_blk = thresh_min_blk;
		m_thresh_max_blk = thresh_max_blk;
		m_thresh_min_wht = thresh_min_wht;
		m_thresh_max_wht = thresh_max_wht;
		m_black = black;
		m_white = white;
		m_slope_brightness = 1.0f / (white - black);
	};

	inline void WeightAndAddSample(
		const element &center,
		const element &neighbor)
	{
		assert(m_count < m_samples.size()); // Full?
		if (isnan(center) || isnan(neighbor)) return; // Todo: need type independent method
		weight w = 1.0f - std::max(0.0f, std::min(1.0f, (fabs(center - neighbor) - m_thresh_min_blk) * m_slope_weight)); // Todo: need type independent method
		m_samples[m_count].m_weight = w;
		m_samples[m_count].m_value = neighbor * w;
		m_count++;
	};
	inline void WeightAndAddSample(
		const element &center,
		const element &neighbor,
		const element &intensity)
	{
		// Todo: need type independent methods
		assert(m_count < m_samples.size()); // Full?
		if (isnan(center) || isnan(neighbor)) return;
		if (center <= 0) return;
		if (neighbor <= 0) return;
		element cross_fade = std::max(0.0f, std::min(1.0f, intensity - m_black) * m_slope_brightness);
		element thresh_min = cross_fade * (m_thresh_min_wht - m_thresh_min_blk) + m_thresh_min_blk;
		element thresh_max = cross_fade * (m_thresh_max_wht - m_thresh_max_blk) + m_thresh_max_blk;
		weight w = 1.0f - std::max(0.0f, std::min(1.0f, (fabs(center - neighbor) - thresh_min) / (thresh_max - thresh_min)));
		m_samples[m_count].m_weight = w;
		m_samples[m_count].m_value = neighbor * w;
		m_count++;
	};
	inline element Compute() const
	{
		element sum = 0;
		weight wgt = 0;
		for (unsigned int i = 0; i < m_count; i++)
		{
			if (!isnan(m_samples[i].m_value)) // Todo: need type independent method
			{
				sum += m_samples[i].m_value;
				wgt += m_samples[i].m_weight;
			}
		}
		if (wgt > m_weights_threshold) return sum / wgt;
		return NAN;
	};

	unsigned int m_count;
	weight m_weights_threshold;
	element m_black;
	element m_white;
	element m_thresh_min_blk;
	element m_thresh_max_blk;
	element m_thresh_min_wht;
	element m_thresh_max_wht;
	element m_slope_weight;
	element m_slope_brightness;
	std::vector<TvWeightedElement<element, weight>> m_samples;
};

// Kernels for floating point types
// Needs converter for 16 bit fixed point fraction
template <class T>
struct TvKernel3x3
{
	TvKernel3x3(T c0 = (T)0.5, T c1 = (T)0.25) { Set(c0, c1); };
	~TvKernel3x3() {};
	void Set(T c0 = (T)0.5, T c1 = (T)0.25)
	{
		m_kernel[0][0] = (T)(c1 * c1);
		m_kernel[0][1] = (T)(c1 * c0);
		m_kernel[0][2] = (T)(c1 * c1);

		m_kernel[1][0] = (T)(c0 * c1);
		m_kernel[1][1] = (T)(c0 * c0);
		m_kernel[1][2] = (T)(c0 * c1);

		m_kernel[2][0] = (T)(c1 * c1);
		m_kernel[2][1] = (T)(c1 * c0);
		m_kernel[2][2] = (T)(c1 * c1);
	}
	void ReportSums()
	{
		T sum = 0.0;
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				cout << m_kernel[i][j] << " ";
				sum += m_kernel[i][j];
			}
			cout << "\n";
		}
		cout << "Sum = " << sum << "\n";

		sum = 0.0;
		for (int i = 0; i < 3; i += 2)
		{
			for (int j = 0; j < 3; j += 2)
			{
				cout << m_kernel[i][j] << " ";
				sum += m_kernel[i][j];
			}
			cout << "\n";
		}
		cout << "Sum = " << sum << "\n";

		sum = 0.0;
		for (int i = 1; i < 3; i += 2)
		{
			for (int j = 0; j < 3; j += 2)
			{
				cout << m_kernel[i][j] << " ";
				sum += m_kernel[i][j];
			}
			cout << "\n";
		}
		cout << "Sum = " << sum << "\n";

		sum = 0.0;
		for (int i = 0; i < 3; i += 2)
		{
			for (int j = 1; j < 3; j += 2)
			{
				cout << m_kernel[i][j] << " ";
				sum += m_kernel[i][j];
			}
			cout << "\n";
		}
		cout << "Sum = " << sum << "\n";

		sum = 0.0;
		for (int i = 1; i < 3; i += 2)
		{
			for (int j = 1; j < 3; j += 2)
			{
				cout << m_kernel[i][j] << " ";
				sum += m_kernel[i][j];
			}
			cout << "\n";
		}
		cout << "Sum = " << sum << "\n";
	};

	T m_kernel[3][3];
};

template <class T>
struct TvKernel5x5
{
	TvKernel5x5(T c0 = (T)0.4, T c1 = (T)0.25, T c2 = (T)0.05) { Set(c0, c1, c2); };
	~TvKernel5x5() {};
	void Set(T c0 = (T)0.4, T c1 = (T)0.25, T c2 = (T)0.05)
	{
		m_kernel[0][0] = (T)(c2 * c2);
		m_kernel[0][1] = (T)(c2 * c1);
		m_kernel[0][2] = (T)(c2 * c0);
		m_kernel[0][3] = (T)(c2 * c1);
		m_kernel[0][4] = (T)(c2 * c2);

		m_kernel[1][0] = (T)(c1 * c2);
		m_kernel[1][1] = (T)(c1 * c1);
		m_kernel[1][2] = (T)(c1 * c0);
		m_kernel[1][3] = (T)(c1 * c1);
		m_kernel[1][4] = (T)(c1 * c2);

		m_kernel[2][0] = (T)(c0 * c2);
		m_kernel[2][1] = (T)(c0 * c1);
		m_kernel[2][2] = (T)(c0 * c0);
		m_kernel[2][3] = (T)(c0 * c1);
		m_kernel[2][4] = (T)(c0 * c2);

		m_kernel[3][0] = (T)(c1 * c2);
		m_kernel[3][1] = (T)(c1 * c1);
		m_kernel[3][2] = (T)(c1 * c0);
		m_kernel[3][3] = (T)(c1 * c1);
		m_kernel[3][4] = (T)(c1 * c2);

		m_kernel[4][0] = (T)(c2 * c2);
		m_kernel[4][1] = (T)(c2 * c1);
		m_kernel[4][2] = (T)(c2 * c0);
		m_kernel[4][3] = (T)(c2 * c1);
		m_kernel[4][4] = (T)(c2 * c2);
	}
	void SetStd3x3()
	{
		Zero();
		m_kernel[1][1] = 0.0625f;
		m_kernel[1][2] = 0.125f;
		m_kernel[1][3] = 0.0625f;

		m_kernel[2][1] = 0.125f;
		m_kernel[2][2] = 0.025f;
		m_kernel[2][3] = 0.125f;

		m_kernel[3][1] = 0.0625f;
		m_kernel[3][2] = 0.125f;
		m_kernel[3][3] = 0.0625f;
	};
	void Zero()
	{
		for (int i = 0; i < 5; i++)
		{
			for (int j = 0; j < 5; j++)
			{
				m_kernel[i][j] = 0;
			}
		}
	};
	void ReportSums()
	{
		T sum = 0.0;
		for (int i = 0; i < 5; i++)
		{
			for (int j = 0; j < 5; j++)
			{
				cout << m_kernel[i][j] << " ";
				sum += m_kernel[i][j];
			}
			cout << "\n";
		}
		cout << "Sum = " << sum << "\n";

		sum = 0.0;
		for (int i = 0; i < 5; i += 2)
		{
			for (int j = 0; j < 5; j += 2)
			{
				cout << m_kernel[i][j] << " ";
				sum += m_kernel[i][j];
			}
			cout << "\n";
		}
		cout << "Sum = " << sum << "\n";

		sum = 0.0;
		for (int i = 1; i < 5; i += 2)
		{
			for (int j = 0; j < 5; j += 2)
			{
				cout << m_kernel[i][j] << " ";
				sum += m_kernel[i][j];
			}
			cout << "\n";
		}
		cout << "Sum = " << sum << "\n";

		sum = 0.0;
		for (int i = 0; i < 5; i += 2)
		{
			for (int j = 1; j < 5; j += 2)
			{
				cout << m_kernel[i][j] << " ";
				sum += m_kernel[i][j];
			}
			cout << "\n";
		}
		cout << "Sum = " << sum << "\n";

		sum = 0.0;
		for (int i = 1; i < 5; i += 2)
		{
			for (int j = 1; j < 5; j += 2)
			{
				cout << m_kernel[i][j] << " ";
				sum += m_kernel[i][j];
			}
			cout << "\n";
		}
		cout << "Sum = " << sum << "\n";
	};

	T m_kernel[5][5];
};

// Output image must be properly pre-allocated
template <class image_type>
bool ReplaceNans(image_type &with_nans, const image_type &without_nans)
{
	cout << "ReplaceNans with_nans = " << with_nans.Data() << " without_nans = " << without_nans.Data() << "\n";
	if (with_nans.IsEmpty()) return true;
	if (without_nans.IsEmpty()) return true;

	const int width = std::min(with_nans.Width(), without_nans.Width());
	const int height = std::min(with_nans.Height(), without_nans.Height());
	int nans_replaced = 0;

	for (int y = 0; y < height; y++)
	{
		auto *with_nans_row_ptr = with_nans[y];
		const auto *without_nans_row_ptr = without_nans[y];
		for (int x = 0; x < width; x++)
		{
			if (isnan(with_nans_row_ptr[x])) // Todo: need type independent method
			{
				with_nans_row_ptr[x] = without_nans_row_ptr[x];
				nans_replaced++;
			}
		}
	}
	cout << "Nans replaced " << nans_replaced << "\n";
	return false;
};

// Output image must be properly pre-allocated
template <class image_type, class pixel_type>
void Downsampler3x3(const image_type &input, image_type &output)
{
	static const TvKernel3x3<pixel_type> kernel; // Todo: need type independent method
	int ycoords[3], xcoords[3], yout, xout;

	// Declare sum in a type independent manner
	auto sum = input[0][0];

	// Iterate over output image
	for (yout = 0; yout < output.Height(); yout++)
	{
		ycoords[1] = std::min(yout * 2, input.Height() - 1);
		ycoords[0] = std::max(ycoords[1] - 1, 0);
		ycoords[2] = std::min(ycoords[1] + 1, input.Height() - 1);
		for (xout = 0; xout < output.Width(); xout++)
		{
			xcoords[1] = std::min(xout * 2, input.Width() - 1);
			xcoords[0] = std::max(xcoords[1] - 1, 0);
			xcoords[2] = std::min(xcoords[1] + 1, input.Width() - 1);
			sum = 0;
			for (int y = 0; y < 3; y++)
			{
				for (int x = 0; x < 3; x++)
				{
					sum += input[ycoords[y]][xcoords[x]] * kernel.m_kernel[y][x];
				}
			}
			output[yout][xout] = sum; // kernel must be unity gain;
		}
	}
};

// Output image must be properly pre-allocated
template <class image_type, class pixel_type>
int DownsamplerNan3x3(
	const image_type &input,
	image_type &output,
	pixel_type weights_thresh = (pixel_type)0.1f)
{
	static const TvKernel3x3<pixel_type> kernel; // Todo: need type independent method
	TvWeightedAccumulator<pixel_type, pixel_type> wc(3, 1, weights_thresh); // Todo: need type independent method

	int ycoords[3], xcoords[3], nan_count = 0, yout, xout;

	// Iterate over output image
	for (yout = 0; yout < output.Height(); yout++)
	{
		ycoords[1] = std::min(yout * 2, input.Height() - 1);
		ycoords[0] = std::max(ycoords[1] - 1, 0);
		ycoords[2] = std::min(ycoords[1] + 1, input.Height() - 1);
		for (xout = 0; xout < output.Width(); xout++)
		{
			xcoords[1] = std::min(xout * 2, input.Width() - 1);
			xcoords[0] = std::max(xcoords[1] - 1, 0);
			xcoords[2] = std::min(xcoords[1] + 1, input.Width() - 1);
			wc.Reset();
			for (int y = 0; y < 3; y++)
			{
				for (int x = 0; x < 3; x++)
				{
					wc.AddSample(input[ycoords[y]][xcoords[x]], kernel.m_kernel[y][x]);
				}
			}
			output[yout][xout] = wc.Compute();
			if (isnan(output[yout][xout])) nan_count++; // Todo: need type independent method
		}
	}
	return nan_count;
};

// Output image must be properly pre-allocated
template <class image_type, class pixel_type>
void Downsampler5x5(const image_type &input, image_type &output)
{
	static const TvKernel5x5<pixel_type> kernel; // Todo: need type independent method
	int ycoords[5], xcoords[5], yout, xout;

	// Declare sum in a type independent manner
	auto sum = input[0][0];

	// Iterate over output image
	for (yout = 0; yout < output.Height(); yout++)
	{
		ycoords[2] = std::min(yout * 2, input.Height() - 1);
		ycoords[0] = std::max(ycoords[2] - 2, 0);
		ycoords[1] = std::max(ycoords[2] - 1, 0);
		ycoords[3] = std::min(ycoords[2] + 1, input.Height() - 1);
		ycoords[4] = std::min(ycoords[2] + 2, input.Height() - 1);
		for (xout = 0; xout < output.Width(); xout++)
		{
			xcoords[2] = std::min(xout * 2, input.Width() - 1);
			xcoords[0] = std::max(xcoords[2] - 2, 0);
			xcoords[1] = std::max(xcoords[2] - 1, 0);
			xcoords[3] = std::min(xcoords[2] + 1, input.Width() - 1);
			xcoords[4] = std::min(xcoords[2] + 2, input.Width() - 1);
			sum = 0;
			for (int y = 0; y < 5; y++)
			{
				for (int x = 0; x < 5; x++)
				{
					sum += input[ycoords[y]][xcoords[x]] * kernel.m_kernel[y][x];
				}
			}
			output[yout][xout] = sum; // kernel must be unity gain;
		}
	}
};

// Output image must be properly pre-allocated
template <class image_type, class pixel_type>
int DownsamplerNan5x5(
	const image_type &input,
	image_type &output,
	pixel_type weights_thresh = (pixel_type)0.1f)
{
	static TvKernel5x5<pixel_type> kernel; // Todo: need type independent method
	TvWeightedAccumulator<pixel_type, pixel_type> wc(5, 1, weights_thresh); // Todo: need type independent method

	int ycoords[5], xcoords[5], nan_count = 0, yout, xout;

	// Iterate over output image
	for (yout = 0; yout < output.Height(); yout++)
	{
		ycoords[2] = std::min(yout * 2, input.Height() - 1);
		ycoords[0] = std::max(ycoords[2] - 2, 0);
		ycoords[1] = std::max(ycoords[2] - 1, 0);
		ycoords[3] = std::min(ycoords[2] + 1, input.Height() - 1);
		ycoords[4] = std::min(ycoords[2] + 2, input.Height() - 1);
		for (xout = 0; xout < output.Width(); xout++)
		{
			xcoords[2] = std::min(xout * 2, input.Width() - 1);
			xcoords[0] = std::max(xcoords[2] - 2, 0);
			xcoords[1] = std::max(xcoords[2] - 1, 0);
			xcoords[3] = std::min(xcoords[2] + 1, input.Width() - 1);
			xcoords[4] = std::min(xcoords[2] + 2, input.Width() - 1);
			wc.Reset();
			for (int y = 0; y < 5; y++)
			{
				for (int x = 0; x < 5; x++)
				{
					wc.AddSample(input[ycoords[y]][xcoords[x]], kernel.m_kernel[y][x]);
				}
			}
			output[yout][xout] = wc.Compute();
			if (isnan(output[yout][xout])) nan_count++; // Todo: need type independent method
		};
	}
	return nan_count;
};

// Output image must be properly pre-allocated
template <class image_type, class pixel_type>
void Upsampler5x5(const image_type &input, image_type &output)
{
	static const TvKernel5x5<pixel_type> kernel; // Todo: need type independent method

	static pixel_type norm_ul = 1.0f / (
		kernel.m_kernel[2][2] +
		kernel.m_kernel[0][2] +
		kernel.m_kernel[4][2] +
		kernel.m_kernel[2][0] +
		kernel.m_kernel[2][4] +
		kernel.m_kernel[0][0] +
		kernel.m_kernel[0][4] +
		kernel.m_kernel[4][0] +
		kernel.m_kernel[4][4]);
	static pixel_type norm_ur = 1.0f / (
		kernel.m_kernel[2][1] +
		kernel.m_kernel[2][3] +
		kernel.m_kernel[0][1] +
		kernel.m_kernel[0][3] +
		kernel.m_kernel[4][1] +
		kernel.m_kernel[4][3]);
	static pixel_type norm_ll = 1.0f / (
		kernel.m_kernel[1][0] +
		kernel.m_kernel[1][2] +
		kernel.m_kernel[1][4] +
		kernel.m_kernel[3][0] +
		kernel.m_kernel[3][2] +
		kernel.m_kernel[3][4]);
	static pixel_type norm_lr = 1.0f / (
		kernel.m_kernel[1][1] +
		kernel.m_kernel[1][3] +
		kernel.m_kernel[3][1] +
		kernel.m_kernel[3][3]);

	int ycoords[3], xcoords[3], yout, xout;

	// Declare sum and weights in a type independent manner
	auto sum = input[0][0], weights = input[0][0];

	// Iterate over output image
	for (yout = 0; yout < output.Height(); yout += 2)
	{
		ycoords[1] = std::min(yout / 2, input.Height() - 1);
		ycoords[0] = std::max(ycoords[1] - 1, 0);
		ycoords[2] = std::min(ycoords[1] + 1, input.Height() - 1);
		int yout_lower = std::min(yout + 1, output.Height() - 1);
		for (xout = 0; xout < output.Width(); xout += 2)
		{
			xcoords[1] = std::min(xout / 2, input.Width() - 1);
			xcoords[0] = std::max(xcoords[1] - 1, 0);
			xcoords[2] = std::min(xcoords[1] + 1, input.Width() - 1);
			int xout_right = std::min(xout + 1, output.Width() - 1);

			// Note order so the correct value overrides for odd widths and heights
			output[yout_lower][xout_right] = norm_lr * (
				input[ycoords[1]][xcoords[1]] * kernel.m_kernel[1][1] +
				input[ycoords[1]][xcoords[2]] * kernel.m_kernel[1][3] +
				input[ycoords[2]][xcoords[1]] * kernel.m_kernel[3][1] +
				input[ycoords[2]][xcoords[2]] * kernel.m_kernel[3][3]);

			output[yout_lower][xout] = norm_ll * (
				input[ycoords[1]][xcoords[0]] * kernel.m_kernel[1][0] +
				input[ycoords[1]][xcoords[1]] * kernel.m_kernel[1][2] +
				input[ycoords[1]][xcoords[2]] * kernel.m_kernel[1][4] +
				input[ycoords[2]][xcoords[0]] * kernel.m_kernel[3][0] +
				input[ycoords[2]][xcoords[1]] * kernel.m_kernel[3][2] +
				input[ycoords[2]][xcoords[2]] * kernel.m_kernel[3][4]);

			output[yout][xout_right] = norm_ur * (
				input[ycoords[1]][xcoords[1]] * kernel.m_kernel[2][1] +
				input[ycoords[1]][xcoords[2]] * kernel.m_kernel[2][3] +
				input[ycoords[0]][xcoords[1]] * kernel.m_kernel[0][1] +
				input[ycoords[0]][xcoords[2]] * kernel.m_kernel[0][3] +
				input[ycoords[2]][xcoords[1]] * kernel.m_kernel[4][1] +
				input[ycoords[2]][xcoords[2]] * kernel.m_kernel[4][3]);

			output[yout][xout] = norm_ul * ( 
				input[ycoords[1]][xcoords[1]] * kernel.m_kernel[2][2] +
				input[ycoords[0]][xcoords[1]] * kernel.m_kernel[0][2] +
				input[ycoords[2]][xcoords[1]] * kernel.m_kernel[4][2] +
				input[ycoords[1]][xcoords[0]] * kernel.m_kernel[2][0] +
				input[ycoords[1]][xcoords[2]] * kernel.m_kernel[2][4] +
				input[ycoords[0]][xcoords[0]] * kernel.m_kernel[0][0] +
				input[ycoords[0]][xcoords[2]] * kernel.m_kernel[0][4] +
				input[ycoords[2]][xcoords[0]] * kernel.m_kernel[4][0] +
				input[ycoords[2]][xcoords[2]] * kernel.m_kernel[4][4]);
		}
	}
};

// Output image must be properly pre-allocated
template <class image_type, class pixel_type>
void UpsamplerNan5x5(const image_type &input, image_type &output, pixel_type weights_thresh = 0.04f)
{
	static TvKernel5x5<pixel_type> kernel; // Todo: need type independent method
//	kernel.SetStd3x3();
//	kernel.ReportSums();
	TvWeightedAccumulator<pixel_type, pixel_type> wc(5, 1, weights_thresh); // Todo: need type independent method

	int ycoords[3], xcoords[3], yout, xout;

	// Iterate over output image
	for (yout = 0; yout < output.Height(); yout += 2)
	{
		ycoords[1] = std::min(yout / 2, input.Height() - 1);
		ycoords[0] = std::max(ycoords[1] - 1, 0);
		ycoords[2] = std::min(ycoords[1] + 1, input.Height() - 1);
		int yout_lower = std::min(yout + 1, output.Height() - 1);
		for (xout = 0; xout < output.Width(); xout += 2)
		{
			xcoords[1] = std::min(xout / 2, input.Width() - 1);
			xcoords[0] = std::max(xcoords[1] - 1, 0);
			xcoords[2] = std::min(xcoords[1] + 1, input.Width() - 1);
			int xout_right = std::min(xout + 1, output.Width() - 1);

			// Note order so the correct value overrides for odd widths and heights
			wc.Reset();
			wc.AddSample(input[ycoords[1]][xcoords[1]], kernel.m_kernel[1][1]);
			wc.AddSample(input[ycoords[1]][xcoords[2]], kernel.m_kernel[1][3]);
			wc.AddSample(input[ycoords[2]][xcoords[1]], kernel.m_kernel[3][1]);
			wc.AddSample(input[ycoords[2]][xcoords[2]], kernel.m_kernel[3][3]);
			output[yout_lower][xout_right] = wc.Compute();

			wc.Reset();
			wc.AddSample(input[ycoords[1]][xcoords[0]], kernel.m_kernel[1][0]);
			wc.AddSample(input[ycoords[1]][xcoords[1]], kernel.m_kernel[1][2]);
			wc.AddSample(input[ycoords[1]][xcoords[2]], kernel.m_kernel[1][4]);
			wc.AddSample(input[ycoords[2]][xcoords[0]], kernel.m_kernel[3][0]);
			wc.AddSample(input[ycoords[2]][xcoords[1]], kernel.m_kernel[3][2]);
			wc.AddSample(input[ycoords[2]][xcoords[2]], kernel.m_kernel[3][4]);
			output[yout_lower][xout] = wc.Compute();

			wc.Reset();
			wc.AddSample(input[ycoords[1]][xcoords[1]], kernel.m_kernel[2][1]);
			wc.AddSample(input[ycoords[1]][xcoords[2]], kernel.m_kernel[2][3]);
			wc.AddSample(input[ycoords[0]][xcoords[1]], kernel.m_kernel[0][1]);
			wc.AddSample(input[ycoords[0]][xcoords[2]], kernel.m_kernel[0][3]);
			wc.AddSample(input[ycoords[2]][xcoords[1]], kernel.m_kernel[4][1]);
			wc.AddSample(input[ycoords[2]][xcoords[2]], kernel.m_kernel[4][3]);
			output[yout][xout_right] = wc.Compute();

			wc.Reset();
			wc.AddSample(input[ycoords[1]][xcoords[1]], kernel.m_kernel[2][2]);
			wc.AddSample(input[ycoords[0]][xcoords[1]], kernel.m_kernel[0][2]);
			wc.AddSample(input[ycoords[2]][xcoords[1]], kernel.m_kernel[4][2]);
			wc.AddSample(input[ycoords[1]][xcoords[0]], kernel.m_kernel[2][0]);
			wc.AddSample(input[ycoords[1]][xcoords[2]], kernel.m_kernel[2][4]);
			wc.AddSample(input[ycoords[0]][xcoords[0]], kernel.m_kernel[0][0]);
			wc.AddSample(input[ycoords[0]][xcoords[2]], kernel.m_kernel[0][4]);
			wc.AddSample(input[ycoords[2]][xcoords[0]], kernel.m_kernel[4][0]);
			wc.AddSample(input[ycoords[2]][xcoords[2]], kernel.m_kernel[4][4]);
			output[yout][xout] = wc.Compute();
		}
	}
};

// Output image must be properly pre-allocated
template <class image_type>
void UpsamplerBilinear(const image_type &input, image_type &output)
{
	int ycoords[3], xcoords[3], yout, xout;

	// Declare sum and weights in a type independent manner
	auto sum = input[0][0], weights = input[0][0];

	// Iterate over output image
	for (yout = 0; yout < output.Height(); yout += 2)
	{
		ycoords[1] = std::min(yout / 2, input.Height() - 1);
		ycoords[0] = std::max(ycoords[1] - 1, 0);
		ycoords[2] = std::min(ycoords[1] + 1, input.Height() - 1);
		int yout_lower = std::min(yout + 1, output.Height() - 1);
		for (xout = 0; xout < output.Width(); xout += 2)
		{
			xcoords[1] = std::min(xout / 2, input.Width() - 1);
			xcoords[0] = std::max(xcoords[1] - 1, 0);
			xcoords[2] = std::min(xcoords[1] + 1, input.Width() - 1);
			int xout_right = std::min(xout + 1, output.Width() - 1);

			// Note order so the correct value overrides for odd widths and heights
			output[yout_lower][xout_right] = (
				input[ycoords[1]][xcoords[1]] * 0.25f +
				input[ycoords[1]][xcoords[2]] * 0.25f +
				input[ycoords[2]][xcoords[1]] * 0.25f +
				input[ycoords[2]][xcoords[2]] * 0.25f);

			output[yout_lower][xout] = (
				input[ycoords[1]][xcoords[1]] * 0.5f +
				input[ycoords[2]][xcoords[1]] * 0.5f);

			output[yout][xout_right] = (
				input[ycoords[1]][xcoords[1]] * 0.5f +
				input[ycoords[1]][xcoords[2]] * 0.5f);

			output[yout][xout] = input[ycoords[1]][xcoords[1]];
		}
	}
};

// Output image must be properly pre-allocated
template <class image_type, class pixel_type>
void UpsamplerNanBilinear(
	const image_type &input,
	image_type &output,
	pixel_type weights_thresh = 0.04f)
{
	TvWeightedAccumulator<pixel_type, pixel_type> wc(5, 1, weights_thresh); // Todo: need type independent method

	int ycoords[3], xcoords[3], yout, xout;

	// Declare sum and weights in a type independent manner
	auto sum = input[0][0], weights = input[0][0];

	// Iterate over output image
	for (yout = 0; yout < output.Height(); yout += 2)
	{
		ycoords[1] = std::min(yout / 2, input.Height() - 1);
		ycoords[0] = std::max(ycoords[1] - 1, 0);
		ycoords[2] = std::min(ycoords[1] + 1, input.Height() - 1);
		int yout_lower = std::min(yout + 1, output.Height() - 1);
		for (xout = 0; xout < output.Width(); xout += 2)
		{
			xcoords[1] = std::min(xout / 2, input.Width() - 1);
			xcoords[0] = std::max(xcoords[1] - 1, 0);
			xcoords[2] = std::min(xcoords[1] + 1, input.Width() - 1);
			int xout_right = std::min(xout + 1, output.Width() - 1);

			// Note order so the correct value overrides for odd widths and heights
			wc.Reset();
			wc.AddSample(input[ycoords[1]][xcoords[1]], 0.25f);
			wc.AddSample(input[ycoords[1]][xcoords[2]], 0.25f);
			wc.AddSample(input[ycoords[2]][xcoords[1]], 0.25f);
			wc.AddSample(input[ycoords[2]][xcoords[2]], 0.25f);
			output[yout_lower][xout_right] = wc.Compute();

			wc.Reset();
			wc.AddSample(input[ycoords[1]][xcoords[1]], 0.5f);
			wc.AddSample(input[ycoords[2]][xcoords[1]], 0.5f);
			output[yout_lower][xout] = wc.Compute();

			wc.Reset();
			wc.AddSample(input[ycoords[1]][xcoords[1]], 0.5f);
			wc.AddSample(input[ycoords[1]][xcoords[2]], 0.5f);
			output[yout][xout_right] = wc.Compute();

			output[yout][xout] = input[ycoords[1]][xcoords[1]];
		}
	}
};

// Output image must be properly pre-allocated
template <class image_type, class pixel_type>
int Downsampler(
	const image_type &input,
	image_type &output,
	bool do5x5 = true,
	bool handle_nans = true,
	pixel_type weights_thresh = 0.1f)
{
	if (do5x5)
	{
		if (handle_nans) return DownsamplerNan5x5(input, output, weights_thresh);
		Downsampler5x5<image_type, pixel_type>(input, output);
	}
	else
	{
		if (handle_nans) return DownsamplerNan3x3(input, output, weights_thresh);
		Downsampler5x5<image_type, pixel_type>(input, output);
	}
	return 0;
};

// Output image must be properly pre-allocated
template <class image_type, class pixel_type>
void Upsampler(
	const image_type &input,
	image_type &output,
	bool do5x5 = true,
	bool handle_nans = true,
	pixel_type weights_thresh = 0.04f)
{
	cout << "Upsampler input = " << input.Data() << " Output = " << output.Data() << "\n";
	if (do5x5)
	{
		if (handle_nans) UpsamplerNan5x5(input, output, weights_thresh);
		else Upsampler5x5<image_type, pixel_type>(input, output);
	}
	else
	{
		if (handle_nans) return UpsamplerNanBilinear(input, output, weights_thresh);
		else UpsamplerNanBilinear<image_type, pixel_type>(input, output);
	}
};

// Output image must be properly pre-allocated
template <class image_type>
void Fleas(image_type &image, int flea_count, int max_height, int max_width)
{
	std::random_device rd;  //Will be used to obtain a seed for the random number engine
	std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()

	std::uniform_int_distribution<> ygen(0, image.Height() - 1);
	std::uniform_int_distribution<> xgen(0, image.Width() - 1);
	std::uniform_int_distribution<> hgen(1, max_height);
	std::uniform_int_distribution<> wgen(1, max_width);

	for (int i = 0; i < flea_count; i++)
	{
		int y = ygen(gen);
		int x = xgen(gen);
		int h = hgen(gen);
		int w = wgen(gen);
		for (int hh = 0; hh < h; hh++)
		{
			for (int ww = 0; ww < w; ww++)
			{
				image[std::min(image.Height() - 1, y + hh)][std::min(image.Width() - 1, x + ww)] = NAN;
			}
		}
	}
};

// Output image must be properly pre-allocated
// Non-Gaussian distance distribution
template<class image_type, class pixel_type>
bool BilateralNan(
	const image_type &input,
	image_type &output,
	int winsize,
	pixel_type thresh_min_blk,
	pixel_type thresh_max_blk)
{
	cout << "BilateralNan input = " << input.Data() << " Output = " << output.Data() << "\n";
	if (input.IsEmpty()) return true;

	// Can use simple accumulator for better perf
	TvWeightedAccumulator<pixel_type, pixel_type> wc(winsize, 1, 0.001f); // Todo: need type independent method
	wc.SetThresholds(thresh_min_blk, thresh_max_blk);

	const int radius = (winsize / 2);
	const int width_in = input.Width();
	const int height_in = input.Height();
	const int width_out = output.Width();
	const int height_out = output.Height();
	const int width = std::min(width_in, width_out);
	const int height = std::min(height_in, height_out);

	for (int y = 0; y < height; y++)
	{
		const int y_start = std::max(0, y - radius);
		const int y_stop = std::min(height_in - 1, y + radius);
		const pixel_type *center_row_ptr = input[y];
		pixel_type *out_row_ptr = output[y];
		{
			for (int x = 0; x < width; x++)
			{
				auto center = center_row_ptr[x];
				if (isnan(center)) out_row_ptr[x] = center; // Todo: need type independent method
				else
				{
					const int x_start = std::max(0, x - radius);
					const int x_stop = std::min(width_in - 1, x + radius);
					wc.Reset();
					for (int i = y_start; i <= y_stop; i++)
					{
						const pixel_type *in_row_ptr = input[i];
						for (int j = x_start; j <= x_stop; j++)
						{
							wc.WeightAndAddSample(center, in_row_ptr[j]);
						}
					}
					out_row_ptr[x] = wc.Compute();
				}
			}
		}
	}
	return false;
}

// Output image must be properly pre-allocated
// Non-Gaussian distance distribution
// Uses guide image for threshold calculation
template<class image_type, class pixel_type>
bool BilateralNan(
	const image_type &input,
	const image_type &guide,
	image_type &output,
	int winsize,
	pixel_type black,
	pixel_type white,
	pixel_type thresh_min_blk, // Todo: need type independent method
	pixel_type thresh_max_blk,
	pixel_type thresh_min_wht,
	pixel_type thresh_max_wht)
{
	if (input.IsEmpty()) return true;

	// Can use simple accumulator for better perf
	TvWeightedAccumulator<pixel_type, pixel_type> wc(winsize, 1, 0.001f); // Todo: need type independent method
	wc.SetThresholds(black, white, thresh_min_blk, thresh_max_blk, thresh_min_wht, thresh_max_wht);

	const int radius = (winsize / 2);
	const int width_in = input.Width();
	const int height_in = input.Height();
	const int width_out = output.Width();
	const int height_out = output.Height();
	const int width = std::min(width_in, width_out);
	const int height = std::min(height_in, height_out);

	for (int y = 0; y < height; y++)
	{
		const int y_start = std::max(0, y - radius);
		const int y_stop = std::min(height_in - 1, y + radius);
		const pixel_type *center_row_ptr = input[y];
		const pixel_type *guide_row_ptr = guide[y];
		pixel_type *out_row_ptr = output[y];
		{
			for (int x = 0; x < width; x++)
			{
				auto center = center_row_ptr[x];
				auto guide_value = guide_row_ptr[x];
				if (isnan(center)) out_row_ptr[x] = center; // Todo: need type independent method
				else
				{
					const int x_start = std::max(0, x - radius);
					const int x_stop = std::min(width_in - 1, x + radius);
					wc.Reset();
					for (int i = y_start; i <= y_stop; i++)
					{
						const pixel_type *in_row_ptr = input[i];
						for (int j = x_start; j <= x_stop; j++)
						{
							wc.WeightAndAddSample(center, in_row_ptr[j], guide_value);
						}
					}
					out_row_ptr[x] = wc.Compute();
				}
			}
		}
	}
	return false;
}

template <class image_type, class pixel_type>
int RangeAndIntensity(
	const image_type &c1,
	const image_type &c2,
	image_type &range,
	image_type &intensity,
	pixel_type cmin,
	pixel_type cmax,
	pixel_type imin,
	pixel_type imax,
	pixel_type gamma = 0.45f)
{
	if (c1.IsEmpty()) return 0;
	if (c2.IsEmpty()) return 0;

	range.Allocate(c1);
	intensity.Allocate(c1);

	const int width = std::min(c1.Width(), c2.Width());
	const int height = std::min(c1.Height(), c2.Height());
	const bool no_gamma = (gamma == 1.0f);
	int nan_count = 0;

	// Todo: need type independent methods
	for (int y = 0; y < height; y++)
	{
		auto *range_ptr = range[y];
		auto *intensity_ptr = intensity[y];
		const auto *c1_ptr = c1[y];
		const auto *c2_ptr = c2[y];
		for (int x = 0; x < width; x++)
		{
			bool nan = false;
			auto cc1 = c1_ptr[x];
			if ((cc1 < cmin) || (cc1 > cmax) || isnan(cc1))
			{
				cc1 = std::min(cmax, std::max(cmin, cc1));
				nan = true;
			}
			auto cc2 = c2_ptr[x];
			if ((cc2 < cmin) || (cc2 > cmax) || isnan(cc2))
			{
				cc2 = std::min(cmax, std::max(cmin, cc2));
				nan = true;
			}
			auto brightness = cc1 + cc2;
			if ((brightness < imin) || (brightness > imax))
			{
				brightness = std::min(imax, std::max(imin, brightness));
				nan = true;
			}
			if (no_gamma) intensity_ptr[x] = brightness;
			else intensity_ptr[x] = powf((brightness - imin) / (imax - imin), gamma) * (imax - imin) + imin;
			if (nan)
			{
				range_ptr[x] = NAN;
				nan_count++;
			}
			else range_ptr[x] = cc1 / brightness;
		}
	}
	cout << "RangeAndIntensity nan count " << nan_count << "\n";
	return nan_count;
};


template <class image_type, class pixel_type>
bool TransferDifferences(
	const image_type &hi_frequency,
	const image_type &lo_frequency,
	image_type &transfer)
{
	if (hi_frequency.IsEmpty()) return true;
	if (lo_frequency.IsEmpty()) return true;
	if (transfer.IsEmpty()) return true;

	const int width = std::min(hi_frequency.Width(), lo_frequency.Width());
	const int height = std::min(hi_frequency.Height(), lo_frequency.Height());
	int nan_count = 0;

	for (int y = 0; y < height; y++)
	{
		auto *transfer_ptr = transfer[y];
		const auto *hi_frequency_ptr = hi_frequency[y];
		const auto *lo_frequency_ptr = lo_frequency[y];
		for (int x = 0; x < width; x++)
		{
			if (isnan(hi_frequency_ptr[x]) || isnan(lo_frequency_ptr[x]) || isnan(transfer_ptr[x])) transfer_ptr[x] = NAN;
			else transfer_ptr[x] = transfer_ptr[x] + (hi_frequency_ptr[x] - lo_frequency_ptr[x]);
		}
	}
	cout << "Nan count " << nan_count << "\n";
	return false;
};

enum MosaicPolarity
{
	e_c1c2 = 0,
	e_c2c1,
};

template<class image_type, class pixel_type>
bool MakeMosaic(
	const image_type &c1,
	const image_type &c2,
	image_type &output,
	MosaicPolarity polarity)
{
	if (c1.IsEmpty()) return true;
	if (c2.IsEmpty()) return true;
	output.Allocate(c1);

	const bool c1ul = polarity == e_c1c2;
	const int width = std::min(c1.Width(), c2.Width());
	const int height = std::min(c1.Height(), c2.Height());

	for (int y = 0; y < height; y++)
	{
		auto *output_ptr = output[y];
		const auto *c1_ptr = c1[y];
		const auto *c2_ptr = c2[y];
		const bool even_row = (y & 1) == 0;
		for (int x = 0; x < width; x++)
		{
			const bool even_col = (x & 1) == 0;
			const bool ullr = even_row == even_col;
			if (ullr && c1ul) output_ptr[x] = c1_ptr[x];
			else output_ptr[x] = c2_ptr[x];
		}
	}
	return false;
};

template<class image_type, class pixel_type>
bool DemosaicAverage(
	const image_type &mosiac,
	image_type &c1,
	image_type &c2,
	MosaicPolarity polarity)
{
	if (mosiac.IsEmpty()) return true;
	c1.Allocate(mosiac);
	c2.Allocate(mosiac);

	const bool c1ul = polarity == e_c1c2;
	const int width = mosiac.Width();
	const int height = mosiac.Height();

	for (int y = 0; y < height; y++)
	{
		const auto *mosiac_ptr = mosiac[y];
		const auto *mosiac_m1_ptr = mosiac[std::max(0, y - 1)];
		const auto *mosiac_p1_ptr = mosiac[std::min(height - 1, y + 1)];
		auto *c1_ptr = c1[y];
		auto *c2_ptr = c2[y];
		const bool even_row = (y & 1) == 0;
		for (int x = 0; x < width; x++)
		{
			const bool even_col = (x & 1) == 0;
			const bool ullr = even_row == even_col;
			pixel_type weight = (pixel_type)0, ave = (pixel_type)0, pixel = mosiac_ptr[x];

			// Unbiased average
			if (x > 0)
			{
				ave += mosiac_ptr[x - 1];
				weight += (pixel_type)1;
			}
			if (x < width - 1)
			{
				ave += mosiac_ptr[x + 1];
				weight += (pixel_type)1;
			}
			if (y > 0)
			{
				ave += mosiac_m1_ptr[x];
				weight += (pixel_type)1;
			}
			if (y < height - 1)
			{
				ave += mosiac_p1_ptr[x];
				weight += (pixel_type)1;
			}
			if (weight > (pixel_type)0) ave /= weight;
			else ave = NAN;

			// Output
			if (ullr && c1ul)
			{
				c1_ptr[x] = pixel;
				c2_ptr[x] = ave;
			}
			else
			{
				c1_ptr[x] = ave;
				c2_ptr[x] = pixel;
			}
		}
	}
	return false;
};

#endif // TV_FILTER_H
