#ifndef TV_IMAGE_H
#define TV_IMAGE_H

#include <iostream>
#include <fstream>
#include <ostream>
#include <algorithm>
#include <cassert>
#include <type_traits>   

using namespace std;

// Pixel template
template <class value_type, int channels>
struct TvPixel
{
	enum Channels
	{
		e_x = 0,
		e_r = 0,
		e_luma = 0, // Luma for yuv

		e_y = 1, // Not luma for yuv
		e_g = 1,
		e_u = 1,

		e_z = 2,
		e_b = 2,
		e_v = 2,

		e_w = 3,
		e_alpha = 3,
		e_key = 3,
	};

	TvPixel() {};
	TvPixel(value_type value) { SetAll(value); };
	~TvPixel() {};

	inline void SetAll(value_type value) { for (int i = 0; i < channels; i++) m_channel[i] = value; };
	inline void Set(value_type value, int channel = 0) { assert(ValidChannel(channel)); m_channel[channel] = value; };
	inline value_type Get(value_type value, int channel = 0) const { assert(ValidChannel(channel)); return m_channel[channel]; };
	inline bool ValidChannel(int channel) const { return ((channel >= 0) && (channel < channels)); };
	void Dump() const
	{
		for (int i = 0; i < channels; i++)
		{
			cout << m_channel[i];
			if (i < channels - 1) cout << ",";
		}
	};
	friend ostream& operator<<(ostream& os, const TvPixel& pixel) { pixel.Dump(); return os; };

	union
	{
		value_type m_channel[channels];
		value_type m_x;
	};

	// Todo: operator overloads
	// Overload "=" operator for both pixel and value types
};

// Convenient typedefs
typedef TvPixel<float, 4> TvPixelRGBAFloat;
typedef TvPixel<char, 4> TvPixelRGBAUC;
typedef TvPixel<short, 4> TvPixelRGBAI16;
typedef TvPixel<unsigned short, 4> TvPixelRGBAUI16;

typedef TvPixel<float, 3> TvPixelRGBFloat;
typedef TvPixel<char, 3> TvPixelRGBUC;
typedef TvPixel<short, 3> TvPixelRGBI16;
typedef TvPixel<unsigned short, 3> TvPixelRGBUI16;

// Functions return true on error
template <class pixel_type>
class TvImage
{
public:
	TvImage() { Init(); };
	TvImage(int height, int width) { Init(); Allocate(height, width); };
	TvImage(int height, int width, int stride) { Init(); Allocate(height, width, stride); };
	TvImage(const TvImage &clone, bool copy_image_data = true) { Init(); if (copy_image_data) Clone(clone); else Allocate(clone); };
	TvImage(pixel_type *pixels, int height, int width) { Init(); Access(pixels, height, width); }; // Non-ownership
	TvImage(pixel_type *pixels, int height, int width, int stride) { Init(); Access(pixels, height, width, stride); }; // Non-ownerships
	virtual ~TvImage() { Release(); };

	inline int Height() const { return m_height; };
	inline int Width() const { return m_width; };
	inline int Stride() const { return m_stride; };
	inline bool IsEmpty() const { return (m_height * m_width == 0); };
	inline bool IsValidY(int y) const { return ((y >= 0) && (y < m_height)); };
	inline bool IsValidX(int x) const { return ((x >= 0) && (x < m_width)); };
	inline pixel_type* Data() { return m_data; };
	inline const pixel_type* Data() const { return m_data; };
	inline pixel_type* Row(int y) { assert(IsValidY(y)); return m_data + (y * m_stride); };
	inline const pixel_type* Row(int y) const { assert(IsValidY(y)); return m_data + (y * m_stride); };
	inline pixel_type* Data(int y, int x) { assert(IsValidX(x)); return Row(y) + x; }; // Note height, width order
	inline const pixel_type* Data(int y, int x) const { assert(IsValidX(x)); return Row(y) + x; }; // Note height, width order
	inline pixel_type Pixel(int y, int x) const { return *Data(y, x); }; // Note height, width
	inline void Pixel(int y, int x, const pixel_type &pixel) { *Data(y, x) = pixel; }; // Note width, height
	inline int PixelsAllocated() const { return m_pixels_allocated; };
	inline int BytesAllocated() const { return m_pixels_allocated * sizeof(pixel_type); };
	inline int BytesPerPixel() const { return sizeof(pixel_type); };
	inline int BytesPerImageRow() const { return m_width * sizeof(pixel_type); };
	inline int BytesForActiveArea() const { return m_height * m_stride * sizeof(pixel_type); }

	bool Allocate(int height, int width, int stride)
	{
		// Already allocated exact size?
		// Todo: Can optimize for smaller, new allocations
		if (!IsEmpty() && m_self_owned &&
			(height == m_height) &&
			(width == m_width) &&
			(stride == m_stride)) return false;
		Release();
		int size = height * stride;
		if (size == 0) return false;

		// Could use custom allocator for alignment, tracking, etc.
		// Can enforce stride restrictions here
		m_data = new pixel_type[size];
		if (m_data == NULL) return true;
		m_self_owned = true;
		m_width = width;
		m_height = height;
		m_stride = stride;
		m_pixels_allocated = size;
		return false;
	};
	bool Allocate(int height, int width) { return Allocate(height, width, width); };
	bool Allocate(const TvImage &model) { return Allocate(model.Height(), model.Width(), model.Stride()); }; // Does not copy image data
	void Release()
	{
		if ((m_data) && (m_self_owned)) delete[] m_data;
		Init();
	};
	bool Access(pixel_type *pixels, int height, int width, int stride) // Non-ownership
	{
		Release();
		int size = height * stride;
		if (size == 0) return false;
		if (m_data == NULL) return true;
		m_data = pixels;
		m_self_owned = false;
		m_width = width;
		m_height = height;
		m_stride = stride;
		m_pixels_allocated = 0;
		return false;
	};
	bool Access(pixel_type *pixels, int height, int width) { return Access(pixels, height, width, width); }; // Non-ownership
	bool Clear()
	{
		if (IsEmpty()) return true;
		memset(m_data, 0, m_height * m_stride * sizeof(pixel_type));
		return false;
	}
	bool SetAll(const pixel_type &value)
	{
		if (IsEmpty()) return true;
		for (int y = 0; y < Height(); y++)
		{
			pixel_type *row_pointer = Row(y);
			for (int x = 0; x < Width(); x++) *row_pointer++ = value;
		}
		return false;
	};
	bool Clone(const TvImage &clone)
	{
		if (Allocate(clone.Height(), clone.Width(), clone.Stride())) return true;
		if (IsEmpty()) return false; // No contents to copy
		// Check to see if we can just copy
		if ((Stride() == Width()) && (Stride() == clone.Stride())) { memcpy(Data(), clone.Data(), BytesForActiveArea()); }
		else return Copy(clone);
		return false;
	};
	bool Copy(const TvImage &clone)	// Copy minimum area only
	{
		if (IsEmpty() || clone.IsEmpty()) return true;
		// Check to see if we can just copy unstrided image in one fell swoop
		if ((Stride() == Width()) &&
			(Height() == clone.Height()) &&
			(Width() == clone.Width()) &&
			(Stride() == clone.Stride()))
		{
			memcpy(Data(), clone.Data(), BytesForActiveArea());
		}
		else
		{
			// Copy minimum area only row by row
			// Doesn't copy stride overflow
			const int bytes = std::min(Width(), clone.Width()) * BytesPerPixel();
			for (int y = 0; y < std::min(Height(), clone.Height()); y++)
			{
				memcpy(Row(y), clone.Row(y), bytes);
			}
		}
		return false;
	}

	bool DumpWindow(int y_start, int x_start, int y_stop, int x_stop) const
	{
		if (IsEmpty()) return true;
		const int width = Width();
		const int height = Height();

		y_start = std::max(0, y_start);
		y_stop = std::min(height, y_stop);
		x_start = std::max(0, x_start);
		x_stop = std::min(width, x_stop);
		cout << "Window (y,x) from (" << y_start << "," << x_start << ") thru (" << y_stop - 1 << "," << x_stop - 1 << ")\n";

		for (int i = y_start; i < y_stop; i++)
		{
			const pixel_type *in_row_ptr = (*this)[i];
			for (int j = x_start; j < x_stop; j++)
			{
				cout << "(" << in_row_ptr[j] << ")";
			}
			cout << "\n";
		}
		return false;
	};
	bool DumpWindow(int window, int y, int x) const
	{
		const int radius = (window / 2);
		const int y_start = y - radius;
		const int x_start = x - radius;
		return DumpWindow(y_start, x_start, y_start + window, x_start + window);
	};

	// Returning row pointer allows pixel access such as image[y][x]
	inline pixel_type* operator [](int y) { return Row(y); };
	inline const pixel_type* operator [](int y) const { return Row(y); };
	// Todo: add copy operators, overload operators

	friend ostream& operator<<(ostream& os, const TvImage& image)
	{
		cout << "(" << image.Height() << ";" << image.Width() << ";" << image.Stride() << ")";
		return os;
	};

protected:
	void Init()
	{
		m_width = m_height = m_stride = m_pixels_allocated = 0;
		m_self_owned = false;
		m_data = NULL;
	};

	bool m_self_owned;
	int m_width, m_height, m_stride, m_pixels_allocated;
	pixel_type *m_data;
};

// Convenient typedefs
typedef TvImage<float> TvImageFloat;
typedef TvImage<unsigned char> TvImageUC;
typedef TvImage<short> TvImageI16;
typedef TvImage<unsigned short> TvImageUI16;

typedef TvImage<TvPixelRGBFloat> TvImageRGBFloat;
typedef TvImage<TvPixelRGBUC> TvImageRGBUC;
typedef TvImage<TvPixelRGBI16> TvImageRGBI16;
typedef TvImage<TvPixelRGBUI16> TvImageRGBUI16;

typedef TvImage<TvPixelRGBAFloat> TvImageRGBAFloat;
typedef TvImage<TvPixelRGBAUC> TvImageRGBAUC;
typedef TvImage<TvPixelRGBAI16> TvImageRGBAI16;
typedef TvImage<TvPixelRGBAUI16> TvImageRGBAUI16;

class TvBurHeader
{
public:
	enum Offsets
	{
		e_offset_seq = 0,
		e_offset_time_stamp1 = 4,
		e_offset_time_stamp2 = 8,
		e_offset_frame_id = 12,
		e_offset_height = 13,
		e_offset_width = 17,
		e_offset_encoding = 21,
		e_offset_is_big_endian = 25,
		e_offset_step = 26,
		e_offset_reserved = 30,
	};

	enum Sizes
	{
		e_size_seq = 4,
		e_size_time_stamp1 = 4,
		e_size_time_stamp2 = 4,
		e_size_frame_id = 1,
		e_size_height = 4,
		e_size_width = 4,
		e_size_encoding = 4,
		e_size_is_big_endian = 1,
		e_size_step = 4,
		e_total_size = 256
	};

	// Must be somewhere else?
	/* Text from document does not compile
	enum TvBurEncoding
	{
		BAYER_BGGR16 = 0, BAYER_BGGR8, BAYER_GBRG16, BAYER_GBRG8, BAYER_GBRG16, BAYER_RGGB8, BGR16, BGR8, BGRA16, BGRA8, MONO16, MONO8, RGB16, RGBA16, RGBA8, TYPE_16SC1, TYPE_16SC2, TYPE_16SC3, TYPE_16SC4, TYPE_16UC1, TYPE_16UC2, TYPE_16UC3, TYPE_16UC4, TYPE_32FC1, TYPE_32FC2, TYPE_32FC3, TYPE_32FC4, TYPE_32SC1, TYPE_32SC2, TYPE_32SC3, TYPE_32SC4, TYPE_64FC1, TYPE_64FC2, TYPE_64FC3, TYPE_64FC4, TYPE_8SC1, TYPE_8SC2, TYPE_8SC3, TYPE_8SC4, TYPE_8UC1, TYPE_8UC2, TYPE_8UC3, TYPE_8UC4, YUV422
	};
	*/

	TvBurHeader() {Clear();};
	virtual ~TvBurHeader() {};

	void Clear() { memset(m_header, 0, e_total_size); };

	// Header data is poorly defined for alignment reasons
	int GetSeq() const { int seq = 0; memcpy(&seq, m_header + e_offset_seq, e_size_seq); return seq; };
	void SetSeq(int seq) { memcpy(m_header + e_offset_seq, &seq, e_size_seq); };
	int GetSetTimeStamp1() const { int time_offset_stamp1 = 0; memcpy(&time_offset_stamp1, m_header + e_offset_time_stamp1, e_size_time_stamp1); return time_offset_stamp1; };
	void SetTimeStamp1(int time_offset_stamp1) { memcpy(m_header + e_offset_time_stamp1, &time_offset_stamp1, e_size_time_stamp1); };
	int GetSetTimeStamp2() const { int time_offset_stamp2 = 0; memcpy(&time_offset_stamp2, m_header + e_offset_time_stamp2, e_size_time_stamp2); return time_offset_stamp2; };
	void SetTimeStamp2(int time_offset_stamp2) { memcpy(m_header + e_offset_time_stamp2, &time_offset_stamp2, e_size_time_stamp2); };
	int GetFrameId() const { int frame_offset_id = 0; memcpy(&frame_offset_id, m_header + e_offset_frame_id, e_size_frame_id); return frame_offset_id; };
	void SetFrameId(int frame_offset_id) { memcpy(m_header + e_offset_frame_id, &frame_offset_id, e_size_frame_id); };
	unsigned int GetHeight() const { unsigned int height = 0; memcpy(&height, m_header + e_offset_height, e_size_height); return height; };
	void SetHeight(unsigned int height) { memcpy(m_header + e_offset_height, &height, e_size_height); };
	unsigned int GetWidth() const { unsigned int width = 0; memcpy(&width, m_header + e_offset_width, e_size_width); return width; };
	void SetWidth(unsigned int width) { memcpy(m_header + e_offset_width, &width, e_size_width); };
	unsigned int GetEncoding() const { unsigned int encoding = 0; memcpy(&encoding, m_header + e_offset_encoding, e_size_encoding); return encoding; };
	void SetEncoding(unsigned int encoding) { memcpy(m_header + e_offset_encoding, &encoding, e_size_encoding); };
	int GetIsBigEndian() const { bool is_big_endian = false; memcpy(&is_big_endian, m_header + e_offset_is_big_endian, e_size_is_big_endian); return is_big_endian; };
	void SetIsBigEndian(bool is_big_endian) { memcpy(m_header + e_offset_is_big_endian, &is_big_endian, e_size_is_big_endian); };
	unsigned int GetStep() const { unsigned int step = 0; memcpy(&step, m_header + e_offset_step, e_size_step); return step; };
	void SetStep(unsigned int step) { memcpy(m_header + e_offset_step, &step, e_size_step); };

	unsigned char m_header[e_total_size];
};

// Functions return true on error
template <class pixel_type>
class TvBurImage : public TvImage<pixel_type>
{
public:
	TvBurImage() { Init(); };
	TvBurImage(const char *filename) { Init(); ReadBur(filename); };
	TvBurImage(int height, int width) { Init(); Allocate(height, width, width); };
	TvBurImage(int height, int width, int stride) { Init(); Allocate(height, width, stride); };
	TvBurImage(const TvBurImage &clone) { Init(); ImportHeader(clone); TvImage::Clone((TvImage&)clone); };
	virtual ~TvBurImage() {};

	void ClearHeader() { m_bur_header.Clear(); };
	void ImportHeader(const TvBurImage &image) { memcpy(&m_bur_header, &image.m_bur_header, TvBurHeader::e_total_size); };
	bool ReadBur(const char *filename)
	{
		assert(filename);
		if (filename == NULL) return true;
		ifstream file(filename, ios::in | ios::binary | ios::ate);
		if (!file.is_open())
		{
			cout << "Error opening " << filename << "\n";
			return true;
		}
		file.seekg(0, ios::beg);
		file.read((char*)(&m_bur_header.m_header), TvBurHeader::e_total_size); // Todo: check for header goodness
		Allocate(m_bur_header.GetHeight(), m_bur_header.GetWidth());
		file.seekg(TvBurHeader::e_total_size, ios::beg);
		file.read((char*)Data(), BytesAllocated()); // Todo: do row-by-row for width != stride
		file.close();
		return false;
	};
	// Should be sure header set up first
	bool WriteBur(const char *filename)
	{
		assert(filename != NULL);
		if (filename == NULL) return true;
		ofstream file(filename, ios::out | ios::binary | ios::trunc);
		if (!file.is_open())
		{
			cout << "Error opening " << filename << "\n";
			return true;
		}
		m_bur_header.SetWidth(Width());
		m_bur_header.SetHeight(Height());
		file.write((char*)(&m_bur_header.m_header), TvBurHeader::e_total_size);
		file.write((char*)Data(), BytesAllocated()); // Todo: do row-by-row for width != stride
		file.close();
		return false;
	};

	// Todo: copy operators

	TvBurHeader m_bur_header;
};

typedef TvBurImage<float> TvBurImageRange;
typedef TvBurImage<unsigned char> TvBurImageClassifier;
typedef TvBurImage<unsigned short> TvBurImageIntensity;

#endif // TV_IMAGE_H
