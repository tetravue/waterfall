#include "TvPyramid.h"

using namespace std;

// Select waterfalldev.sln
// Select waterfalldev as startup project
// int = intensity (uint16), rng = range (float), ignore cls

// Output must be pre-loaded with copy of input
bool WaterfallNan(
	const TvBurImageRange &range_in,
	TvBurImageRange &range_out,
	int window,
	float threshold)
{
	if (range_in.IsEmpty()) return true;

	const int radius = (window / 2);
	const int width = range_in.Width();
	const int height = range_in.Height();

	float ff_nan = 0.0f;
	unsigned char *p = (unsigned char *)&ff_nan;
	*p++ = 0xff;
	*p++ = 0xff;
	*p++ = 0xff;
	*p++ = 0xff;

	for (int y = 0; y < height; y++)
	{
		const int y_start = std::max(0, y - radius);
		const int y_stop = std::min(height - 1, y + radius);
		const float *center_row_ptr = range_in[y];
		float *out_row_ptr = range_out[y];
		for (int x = 0; x < width; x++)
		{
			float minval = 999999.0f;
			float maxval = -999999.0f;
			if (!isnan(center_row_ptr[x]))
			{
				const int x_start = std::max(0, x - radius);
				const int x_stop = std::min(width - 1, x + radius);
				for (int i = y_start; i <= y_stop; i++)
				{
					const float *in_row_ptr = range_in[i];
					for (int j = x_start; j <= x_stop; j++)
					{
						float neighbor = in_row_ptr[j];
						if (!isnan(neighbor))
						{
							minval = std::min(minval, neighbor);
							maxval = std::max(maxval, neighbor);
						}
					}
				}
				if (fabs(maxval - minval) > threshold)
				{
//					out_row_ptr[x] = NAN;
					out_row_ptr[x] = ff_nan;
//					range_out.DumpWindow(window, y, x);
				}
//				else range_in.DumpWindow(window, y, x);
			}
		}
	}
	return false;
}

void WaterfallTest()
{
	TvBurImageRange range_in("Without waterfall\\000226_rw0_1035.00_1510962947299_rng.bur"), range_out(range_in);

	cout << "Waterfall Test: Image size " << range_in.Height() << ", " << range_in.Width() << "\n";
	WaterfallNan(range_in, range_out, 3, 0.5f);

	range_out.WriteBur("Processed waterfall\\000226_rw0_1035.00_1510962947299_rng.bur");

	cout << range_in.Pixel(0, 0) << "\n";
	unsigned int value;
	float data = range_in.Pixel(0, 0);
	memcpy(&value, &data, 4);
	cout << value << "\n";
	cout << "Pixel size " << sizeof(TvPixelRGBAFloat) << "\n";

	TvPixelRGBAFloat test_float(2.0f);
	test_float.Set(3.0f, 1);
	test_float.Set(4.0f, 2);
	test_float.Set(5.0f, 3);
	test_float.Dump();
	cout << "\n";

	TvImageRGBAFloat test_rgba(20, 20);
	test_rgba.SetAll(test_float);
	test_rgba.DumpWindow(5, 1, 1);
	test_rgba.DumpWindow(5, 2, 2);
	test_rgba.DumpWindow(4, 0, 0);
	test_rgba.DumpWindow(4, 7, 7);
}

inline int compareFloat(const void * a, const void * b)
{
	if (*(float*)a < *(float*)b) return -1;
	return (int) (*(float*)a > *(float*)b);
}


// Output must be pre-loaded with copy of input
template<int window>
bool MedianFill(
	const TvBurImageRange &range_in,
	TvBurImageRange &range_out)
{
	if (range_in.IsEmpty()) return true;

	constexpr int radius = (window / 2);
	const int width = range_in.Width();
	const int height = range_in.Height();

	float array[window * window];

	for (int y = 0; y < height; y++)
	{
		const int y_start = std::max(0, y - radius);
		const int y_stop = std::min(height - 1, y + radius);
		const float *center_row_ptr = range_in[y];
		float *out_row_ptr = range_out[y];
		for (int x = 0; x < width; x++)
		{
			if (isnan(center_row_ptr[x]))
			{
				int count = 0;
				const int x_start = std::max(0, x - radius);
				const int x_stop = std::min(width - 1, x + radius);
				for (int i = y_start; i <= y_stop; i++)
				{
					const float *in_row_ptr = range_in[i];
					for (int j = x_start; j <= x_stop; j++)
					{
						float neighbor = in_row_ptr[j];
						if (!isnan(neighbor))
						{
							array[count++] = neighbor;
						}
					}
				}
				if (count > 0)
				{
					qsort(array, count, sizeof(float), compareFloat);
					out_row_ptr[x] = array[count / 2];
				}
			}
		}
	}
	return false;
}

void MedianFillTest()
{
	TvBurImageRange range_in("Without waterfall\\000226_rw0_1035.00_1510962947299_rng.bur"), range_out(range_in);

	cout << "MedianFill Test: Image size " << range_in.Height() << ", " << range_in.Width() << "\n";
	MedianFill<11>(range_in, range_out);

	range_out.WriteBur("Processed median\\000226_rw0_1035.00_1510962947299_rng.bur");
}

void PyramidTest()
{
	TvBurImageRange range_in("Processed pyramid\\in_000226_rw0_1035.00_1510962947299_rng.bur"), range_out(range_in);
//	TvBurImageRange range_in("Inpaint Results\\Inpaint3_input_rng.bur"), range_out(range_in);
	//	TvBurImageRange range_in("000189_rw0_1030.00_1509578408590_rng.bur"), range_out(range_in);

	cout << "PyramidTest: Image size " << range_in.Height() << ", " << range_in.Width() << "\n";

	TvKernel3x3<float> kernel3x3;
	kernel3x3.ReportSums();
	TvKernel5x5<float> kernel5x5;
	kernel5x5.ReportSums();

	TvPyramid<TvImage<float>, float, true> pyramid((TvImage<float>)range_in, 9);
	cout << pyramid;

	Fleas(range_in, 4000, 20, 1);
	Fleas(range_in, 4000, 1, 20);
	range_in.WriteBur("Processed pyramid\\fleas_thin_000226_rw0_1035.00_1510962947299_rng.bur");

	(*(pyramid[0]))[0]->Copy(range_in);
	pyramid.InpaintNansAndFilter();
//	cout << pyramid.InpaintNansSingleLevel() << "\n";
	range_out.Copy(*((*(pyramid[0]))[0]));

	range_out.WriteBur("Processed pyramid\\out_000226_rw0_1035.00_1510962947299_rng.bur");
//	range_out.WriteBur("out_000189_rw0_1030.00_1509578408590_rng.bur");
}

void BilateralTest()
{
	TvBurImageRange range_in("Processed pyramid\\in_000226_rw0_1035.00_1510962947299_rng.bur"), range_out(range_in);
//	TvBurImageRange range_in("Inpaint Results\\Inpaint3_input_rng.bur"), range_out(range_in);
	//	TvBurImageRange range_in("000189_rw0_1030.00_1509578408590_rng.bur"), range_out(range_in);

	cout << "BilateralTest: Image size " << range_in.Height() << ", " << range_in.Width() << "\n";

	Fleas(range_in, 4000, 20, 1);
	Fleas(range_in, 4000, 1, 20);
	range_in.WriteBur("Processed pyramid\\fleas_thin_000226_rw0_1035.00_1510962947299_rng.bur");

	BilateralNan<TvImageFloat, float>(range_in, range_out, 7, 0.1f, 1.0f);

	range_out.WriteBur("Processed pyramid\\out_000226_rw0_1035.00_1510962947299_rng.bur");
//	range_out.WriteBur("out_000189_rw0_1030.00_1509578408590_rng.bur");
}

void PipelineTest()
{
	TvBurImageRange c1("Registered1\\registered1_c1.bur");
	TvBurImageRange c2("Registered1\\registered1_c2.bur");
	TvBurImageRange inf(c1);
	TvBurImageRange rn1(c1);
	TvBurImageRange rn2(c1);
	TvBurImageRange rn3(c1);
	TvBurImageRange rn4(c1);
	TvBurImageRange cc1(c1);
	TvBurImageRange cc2(c2);

	cout << "PipelineTest: Image size " << c1.Height() << ", " << c1.Width() << "\n";

	RangeAndIntensity(c1, c2, rn1, inf, 0.0f, 800.0f, 0.0f, 1600.0f);
	rn1.WriteBur("Registered1\\registered1_unf.bur");
	inf.WriteBur("Registered1\\registered1_iuf.bur");

	TvPyramid<TvImage<float>, float, true> filter((TvImage<float>)c1, 7);
//	filter.SetFilteringControls(true, 0.1f, 0.04f, false, true, 999, 5, 5.0f, 10.0f);
	filter.SetFilteringControls(true, 0.1f, 0.04f, false, true, 999, 5, 2.0f, 8.0f, 0.5f);

	(*(filter[0]))[0]->Copy(c1);
	filter.InpaintNansAndFilter();
	cc1.Clone(*((*(filter[0]))[0]));
	cc1.WriteBur("Registered1\\registered1_cc1.bur");

	(*(filter[0]))[0]->Copy(c2);
	filter.InpaintNansAndFilter();
	cc2.Clone(*((*(filter[0]))[0]));
	cc2.WriteBur("Registered1\\registered1_cc2.bur");

	int nan_count = RangeAndIntensity(cc1, cc2, rn1, inf, 0.0f, 800.0f, 0.0f, 1600.0f);

	rn2.Clone(rn1);
//	Fleas(rn2, 4000, 20, 1);
//	Fleas(rn2, 4000, 1, 20);

	TvPyramid<TvImage<float>, float, true> pyramid((TvImage<float>)rn2, 2);

	(*(pyramid[0]))[0]->Copy(rn2);
	pyramid.InpaintNansAndFilter();
	rn3.Clone(*((*(pyramid[0]))[0]));

	BilateralNan<TvImageFloat, float>(rn3, inf, rn4, 7, 6.0f, 10.0f, 0.03f, 0.06f, 0.0001f, 0.0004f);
//	BilateralNan<TvImageFloat, float>(rn3, inf, rn4, 7, 0.0f, 6.0f, 1.0f, 2.0f, 1.0f, 2.0f);

	inf.WriteBur("Registered1\\registered1_inf.bur");
	rn1.WriteBur("Registered1\\registered1_rn1.bur");
	rn2.WriteBur("Registered1\\registered1_rn2.bur");
	rn3.WriteBur("Registered1\\registered1_rn3.bur");
	rn4.WriteBur("Registered1\\registered1_rn4.bur");
}
void MosaicTest()
{
	TvBurImageRange c1("Registered1\\registered1_c1.bur");
	TvBurImageRange c2("Registered1\\registered1_c2.bur");
	TvBurImageRange cm(c1);
	TvBurImageRange ca1(c1);
	TvBurImageRange ca2(c1);

	MakeMosaic<TvImage<float>, float>(c1, c2, cm, MosaicPolarity::e_c1c2);
	DemosaicAverage<TvImage<float>, float>(cm, ca1, ca2, MosaicPolarity::e_c1c2);

	cm.WriteBur("Registered1\\registered1_cm.bur");
	ca1.WriteBur("Registered1\\registered1_ca1.bur");
	ca2.WriteBur("Registered1\\registered1_ca2.bur");
}

int main()
{
//	WaterfallTest();
//	MedianFillTest();
//	PyramidTest();
//	BilateralTest();
//	PipelineTest();
	MosaicTest();
	return 0;
}
