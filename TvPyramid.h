#ifndef TV_PYRAMID_H
#define TV_PYRAMID_H

#include "TvFilter.h"
#include <vector>

using namespace std;

// Functions return true on error

//Tools
template <class T>
void Swap(T &a, T &b) { T t = a; a = b; b = t; }

// A single pyramid level
template <class image_type, class pixel_type>
class TvLevel
{
public:
	enum Useage
	{
		e_main = 0,
		e_upsample,
		e_work,
		e_max_images
	};
	TvLevel() { Init(); };
	virtual ~TvLevel() { Release(); };
	// Todo: Somehow this gets referred to but never used?!
//	TvLevel(const TvLevel& dummy) { assert(false); }; // Disallow copy constructor
//	TvLevel(const TvLevel& dummy) = delete; // Disallow copy constructor
	bool IsEmpty() const { return m_images.size() == 0; }
	void SwapUsage(Useage a, Useage b) { Swap(m_images[a], m_images[b]); };
	void Release()
	{
		for (unsigned int i = 0; i < m_images.size(); i++)
		{
			m_images[i]->Release();
			delete m_images[i];
		}
		m_images.clear();
	};
	bool Allocate(int height, int width, int stride, int images = e_max_images)
	{
		Release();
		m_images.resize(images);
		for (unsigned int i = 0; i < m_images.size(); i++)
		{
			m_images[i] = new image_type;
			if ( (m_images[i] == NULL) || (m_images[i]->Allocate(height, width, stride)))
			{
				Release();
				return true;
			}
		}
		return false;
	};
	bool Allocate(const image_type &model, bool downsize_model, int align_size = 1, int images = e_max_images)
	{
		if (model.IsEmpty()) return true;
		int height = model.Height();
		int width = model.Width();
		int stride = model.Stride();
		if (downsize_model)
		{
			height = (height + 1) / 2;
			width = (width + 1) / 2;
			stride = ((width + align_size - 1) / align_size) * align_size;
		}
		return Allocate(height, width, stride, images);
	};
	friend ostream& operator<<(ostream& os, const TvLevel& level)
	{
		if (level.IsEmpty()) cout << "No images";
		else
		{
//			cout << level.m_images.size() << " images size " << *(level.m_images[0]) << " (" << level.m_thresh_min_blk << ", " << level.m_thresh_max_blk << ")";
		}
		return os;
	};

	// Returning image allows pixel access such as m_images[y][x]
	inline image_type* operator [](int image) { return m_images[image]; };
	inline const image_type* operator [](int image) const { return return m_images[image]; };

	std::vector<image_type*> m_images;

	// For filtering
	pixel_type m_black;
	pixel_type m_white;
	pixel_type m_thresh_min_blk;
	pixel_type m_thresh_max_blk;
	pixel_type m_thresh_min_wht;
	pixel_type m_thresh_max_wht;
	int m_winsize;
	int m_max_filter_levels;

protected:
	void Init()
	{
		m_black = (pixel_type)0;
		m_white = (pixel_type)0;
		m_thresh_min_blk = (pixel_type)0;
		m_thresh_max_blk = (pixel_type)0;
		m_thresh_min_wht = (pixel_type)0;
		m_thresh_max_wht = (pixel_type)0;
		m_max_filter_levels = 0;
		m_winsize = 5;
	};
};

// A multi-level pyramid
template <class image_type, class pixel_type, bool handle_nans>
class TvPyramid
{
	using tvlvl = TvLevel<image_type, pixel_type>;
public:
	TvPyramid() { Init(); };
	TvPyramid(const TvPyramid& dummy) = delete; // Disallow copy constructor
	TvPyramid(const image_type &model, int levels = 0, int align_size = 1, int images_per_level = tvlvl::e_max_images)
	{
		Init();
		Allocate(model, levels, align_size, images_per_level);
	};
	virtual ~TvPyramid() { Release(); };
	bool IsEmpty() const { return m_levels.size() == 0; }
	void Release() { m_levels.clear(); };
	bool Allocate(const image_type &model, int levels = 0, int align_size = 1, int images_per_level = tvlvl::e_max_images)
	{
		Release();
		// Todo: Determine max number of levels based on size
		if (levels <= 0) return true;
		m_levels.resize(levels);
		if (m_levels[0].Allocate(model, false, align_size, images_per_level)) return true;
		for (int i = 1; i < levels; i++)
		{
			if (m_levels[i].Allocate(*(m_levels[i - 1].m_images[0]), true, align_size, images_per_level))
			{
				Release();
				return true;
			}
		}
		PropagateControls();
		return false;
	};

	void SetFilteringControls(
		bool do5x5 = true,
		pixel_type downsample_weights_thresh = (pixel_type)0.1f,
		pixel_type upsample_weights_thresh = (pixel_type)0.04f,
		bool limit_levels_by_nans = true,
		bool replace_nans = true,
		int max_filter_levels = 0,
		int winsize = 5,
		pixel_type thresh_min_blk = (pixel_type)0.0f,
		pixel_type thresh_max_blk = (pixel_type)0.0f,
		float decay = (pixel_type)0.5f)
	{
		m_do5x5 = do5x5;
		m_downsample_weights_thresh = downsample_weights_thresh;
		m_upsample_weights_thresh = upsample_weights_thresh;
		m_limit_levels_by_nans = limit_levels_by_nans;
		m_replace_nans = replace_nans;
		m_max_filter_levels = max_filter_levels;
		m_winsize = winsize;
		m_thresh_min_blk = thresh_min_blk;
		m_thresh_max_blk = thresh_max_blk;
		m_decay = decay;
		PropagateControls();
	};

	// Filtering controls must be set first
	void MakeGaussian()
	{
		for (unsigned int level = 0; level < m_levels.size() - 1; level++)
		{
			Downsampler(
				*(m_levels[level][tvlvl::e_main]),
				*(m_levels[level + 1][tvlvl::e_main]),
				m_do5x5,
				m_handle_nans,
				m_weights_thresh);
		}
	};
	void InpaintNansAndFilter()
	{
		bool found_nans = false;
		unsigned int last_nan_level = 0;
		int size = m_levels.size();

		for (unsigned int level = 0; level < m_levels.size() - 1; level++)
		{
			int nan_count = Downsampler(
				*(m_levels[level][tvlvl::e_main]),
				*(m_levels[level + 1][tvlvl::e_main]),
				m_do5x5,
				true,
				m_downsample_weights_thresh);
			if (nan_count > 0) found_nans |= true;
			cout << "Level " << level + 1 << " nan count " << nan_count << "\n";
			last_nan_level = level + 1;
			if (m_limit_levels_by_nans && (nan_count == 0)) break;
		}
		cout << "Last nan level is " << last_nan_level << "\n";

		// Nan replacement reconstruction
		if (m_replace_nans)
		{
			for (int level = last_nan_level - 1; level >= 0; level--)
			{
				cout << "Upsampling level " << level + 1 << " to " << level << "\n";
				Upsampler(
					*(m_levels[level + 1][tvlvl::e_main]),
					*(m_levels[level][tvlvl::e_upsample]),
					m_do5x5,
					true,
					m_upsample_weights_thresh);
				if (m_replace_nans)
				{
					cout << "Replacement level " << level << "\n";
					ReplaceNans(
						*(m_levels[level][tvlvl::e_main]),
						*(m_levels[level][tvlvl::e_upsample]));
				}
			}
		}

		// Filtering reconstruction
		bool filter_from_work = false;
		if (m_max_filter_levels > 1)
		{
			for (int level = std::min((unsigned int)m_max_filter_levels - 2, m_levels.size() - 2) ; level >= 0; level--)
			{
				cout << "Upsampling level " << level + 1 << " to " << level << "\n";
				Upsampler(
					*(m_levels[level + 1][tvlvl::e_main]),
					*(m_levels[level][tvlvl::e_upsample]),
					m_do5x5,
					true,
					m_upsample_weights_thresh);
				cout << "Filtering level " << level + 1 << " " << m_levels[level + 1].m_thresh_min_blk << " " << m_levels[level + 1].m_thresh_max_blk << "\n";
				if (filter_from_work)
				{
					BilateralNan<image_type, pixel_type>(
						*(m_levels[level + 1][tvlvl::e_work]),
						*(m_levels[level + 1][tvlvl::e_work]),
						m_levels[level + 1].m_winsize,
						m_levels[level + 1].m_thresh_min_blk,
						m_levels[level + 1].m_thresh_max_blk);
				}
				else
				{
					BilateralNan<image_type, pixel_type>(
						*(m_levels[level + 1][tvlvl::e_main]),
						*(m_levels[level + 1][tvlvl::e_work]),
						m_levels[level + 1].m_winsize,
						m_levels[level + 1].m_thresh_min_blk,
						m_levels[level + 1].m_thresh_max_blk);
				}
				filter_from_work = true;
				Upsampler(
					*(m_levels[level + 1][tvlvl::e_work]),
					*(m_levels[level][tvlvl::e_work]),
					m_do5x5,
					true,
					m_upsample_weights_thresh);
				TransferDifferences<image_type, pixel_type>(
					*(m_levels[level][tvlvl::e_main]),
					*(m_levels[level][tvlvl::e_upsample]),
					*(m_levels[level][tvlvl::e_work]));
			}
		}

		// Filter highest frequency
		if (m_max_filter_levels > 0)
		{
			if (filter_from_work)
			{
				BilateralNan<image_type, pixel_type>(
					*(m_levels[0][tvlvl::e_work]),
					*(m_levels[0][tvlvl::e_main]),
					m_levels[0].m_winsize,
					m_levels[0].m_thresh_min_blk,
					m_levels[0].m_thresh_max_blk);
			}
			else
			{
				BilateralNan<image_type, pixel_type>(
					*(m_levels[0][tvlvl::e_main]),
					*(m_levels[0][tvlvl::e_work]),
					m_levels[0].m_winsize,
					m_levels[0].m_thresh_min_blk,
					m_levels[0].m_thresh_max_blk);
				Swap(
					m_levels[0].m_images[tvlvl::e_main],
					m_levels[0].m_images[tvlvl::e_work]);
			}
		}
	};

	friend ostream& operator<<(ostream& os, const TvPyramid& pyramid)
	{
		if (pyramid.IsEmpty())
		{
			cout << "Empty pyramid\n";
		}
		else
		{
			cout << "Pyramid\n";
			for (unsigned int i = 0; i < pyramid.m_levels.size(); i++)
			{
				cout << "  Level " << i << " has " << pyramid.m_levels[i] << "\n";
				for (unsigned int j = 0; j < pyramid.m_levels[i].m_images.size(); j++)
				{
					image_type *image = pyramid.m_levels[i].m_images[j];
					cout << "    Image " << j << " at " << image->Data() << " is " << *image << "\n";
				}
			}
		}
		return os;
	};

	// Returning level pointer allows pixel access such as (*((*(pyramid[level]))[image]))[y][x]
	inline tvlvl* operator [](int level) { return &(m_levels[level]); };
	inline const tvlvl* operator [](int level) const { return return &(m_levels[level]); };

	int m_winsize;
	float m_decay;
	bool m_do5x5;
	pixel_type m_downsample_weights_thresh;
	pixel_type m_upsample_weights_thresh;
	bool m_limit_levels_by_nans;
	bool m_replace_nans;
	int m_max_filter_levels;
	pixel_type m_thresh_min_blk;
	pixel_type m_thresh_max_blk;
	std::vector<tvlvl> m_levels;

protected:
	void Init() { SetFilteringControls(); };
	void PropagateControls()
	{
		if (m_levels.size() == 0) return;
		tvlvl *level_ptr_lo, *level_ptr_hi = &(m_levels[0]);

		level_ptr_hi->m_thresh_min_blk = m_thresh_min_blk;
		level_ptr_hi->m_thresh_max_blk = m_thresh_max_blk;
		level_ptr_hi->m_winsize = m_winsize;

		for (unsigned int level = 0; level < m_levels.size() - 1; level++)
		{
			level_ptr_hi = &(m_levels[level]);
			level_ptr_lo = &(m_levels[level + 1]);
			level_ptr_lo->m_thresh_min_blk = level_ptr_hi->m_thresh_min_blk * m_decay;
			level_ptr_lo->m_thresh_max_blk = level_ptr_hi->m_thresh_max_blk * m_decay;
			level_ptr_lo->m_winsize = level_ptr_hi->m_winsize;
		}
	}
};

#endif // TV_PYRAMID_H
